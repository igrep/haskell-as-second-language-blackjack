# haskell-as-second-language-blackjack

エンジニアHubの「まずは型を定義せよ！ アプリケーション開発を通してHaskellプログラミングを学ぼう【第二言語】」という記事で取り上げたサンプル用ライブラリーです。  
ブラックジャックの手札から、合計点数を計算する関数などを提供します。

## 動作を確認したくなったら

記事でも紹介した[stack](https://haskellstack.org/)をインストールした上で、

```bash
git clone https://gitlab.com/igrep/haskell-as-second-language-blackjack.git
cd haskell-as-second-language-blackjack
stack test
```

を実行すれば本ライブラリーのテスト([test/Spec.hs](https://gitlab.com/igrep/haskell-as-second-language-blackjack/blob/master/test/Spec.hs))が実行できます。  
まだ実行に必要なGHCがインストールされていない場合、`stack setup`も事前に実行する必要があります。  
stackの基本的な使い方については[Stackでやる最速Haskell Hello world! (GHCのインストール付き！) - Qiita](http://qiita.com/igrep/items/da1d8df6d40eb001a561)もご覧ください。

また、

```bash
stack ghci
```

を実行すれば、<src/BlackJack.hs>が最初から読み込まれた状態で、GHCiを起動できます。

## 気になったところが出てきたら

[GitLabのIssue](https://gitlab.com/igrep/haskell-as-second-language-blackjack/issues)や、[Haskell-jpの公式サイト](https://haskell.jp/)に載っている[subreddit](https://www.reddit.com/r/haskell_jp/)や、[Slackチーム](https://join-haskell-jp-slack.herokuapp.com/)を通してご連絡ください。  
<whosekiteneverfly@gmail.com>に直接メールいただいてもかまいません。
