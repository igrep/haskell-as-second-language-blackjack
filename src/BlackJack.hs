{-|
  Description: ブラックジャックの手札の合計点数を計算するmodule
-}
module BlackJack
  (
  -- * exportする関数や、型、値コンストラクターの一覧。
  -- | この module を import した場合、この一覧に書いた関数・型・値コンストラクターしか使えない。

  -- | 型と値コンストラクターをexportするときはこのように書く。
  --   今回は「N」以外の値コンストラクターのみをexportすることで、
  --   利用者に任意の数字のCardを作らせないようにする。
  --   詳しくは第3回取り上げる予定。
    Card(A, J, Q, K)

  , cardForTestData
  , deck
  , heartSuit
  , diaSuit
  , cloverSuit
  , spadeSuit
  , sumHand
  ) where


{-|
  本編で紹介した、Card型。
-}
data Card =
  A | N Int | J | Q | K deriving (Eq, Show)


{-|
  本編で紹介し、手札から一番望ましい合計点数を計算する関数。
-}
sumHand :: [Card] -> Int
sumHand cards =
  let possiblePoints = map toPoint cards
      scoreCandidates = foldl plusEach [0] possiblePoints
      noBust = filter (<= 21) scoreCandidates
  in
    if null noBust
      then head scoreCandidates
      else maximum noBust


{-|
  本編で紹介した、Cardを取り得る点数のリストに変換する関数。
-}
toPoint :: Card -> [Int] -- 関数の型の宣言
toPoint A = [1, 11]      -- Aの場合は1と11を取り得る
toPoint (N n) = [n]      -- 通常の数字のカードであれば、カードに書かれた数字のみを取り得る
toPoint _ = [10]         -- そのほかの場合は10のみをとる


{-|
  本編で紹介した、「取り得る各値」をそれぞれ合計する関数。
-}
plusEach :: [Int] -> [Int] -> [Int]
plusEach list1 list2 =
  concatMap (\element1 -> -- 一つ目のリストの各要素を、
    map (\element2 -> -- 二つ目のリストの各要素と、
      element1 + element2 -- 足し合わせる
    ) list2
  ) list1


{-|
  テスト用にCard型の値を作る関数。
  詳細は第3回で取り上げる予定。
-}
cardForTestData :: Int -> Card
cardForTestData = N


-- * このmoduleの利用者が実際に使用すべき、Card型の値を取得するためのリスト
{-|
  詳細は第3回で取り上げる予定。
-}

deck :: [Card]
deck =
  heartSuit
    ++ diaSuit
    ++ cloverSuit
    ++ spadeSuit


suit, heartSuit, diaSuit, cloverSuit, spadeSuit :: [Card]
suit = [A] ++ map N [2..10] ++ [J, Q, K]

heartSuit = suit
diaSuit = suit
cloverSuit = suit
spadeSuit = suit
