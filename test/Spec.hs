module Main where

import Test.Hspec
import BlackJack



{-|
  BlackJack moduleのテスト
-}
main :: IO ()
main = hspec $ do
  describe "BlackJack.deck" $
    it "has 52 cards" $
      length deck `shouldBe` 52

  describe "BlackJack.sumHand" $ do
    it "given empty cards, returns 0" $
      sumHand [] `shouldBe` 0

    it "given ordinary number cards, returns its sum" $
      sumHand [cardForTestData 3, cardForTestData 2] `shouldBe` 5

    it "given cards with Jack, Queen, or King, they are counted as 10 point" $ do
      sumHand [cardForTestData 2, J] `shouldBe` 12
      sumHand [Q, cardForTestData 3] `shouldBe` 13
      sumHand [K, cardForTestData 4] `shouldBe` 14
      sumHand [J, Q] `shouldBe` 20
      sumHand [Q, K] `shouldBe` 20
      sumHand [K, J] `shouldBe` 20

    it "given cards with Ace, Ace is counted as 1 or 11 point" $ do
      sumHand [cardForTestData 2, A] `shouldBe` 13
      sumHand [J, A] `shouldBe` 21
      sumHand [J, cardForTestData 10, A] `shouldBe` 21
      sumHand [K, cardForTestData 5, A, cardForTestData 5] `shouldBe` 21
      sumHand [A, K, cardForTestData 3, A] `shouldBe` 15

    it "given cards whose score exceeds 21, returns score larger than 21" $ do
      sumHand [cardForTestData 10, Q, cardForTestData 2] `shouldSatisfy` (\x -> x > 21)
      sumHand [A, cardForTestData 1, K, K] `shouldSatisfy` (\x -> x > 21)
